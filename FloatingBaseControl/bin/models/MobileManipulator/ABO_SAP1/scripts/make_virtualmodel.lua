local nameDevice	= {}
local nameJoint		= {}
local nameBody		= {}

-- Set Body Name
nameBody[1]			= "Base"
nameBody[2]			= "SBase-4"

-- Set Joint Name
nameJoint[1]	= "Link01-3_joint"
nameJoint[2]	= "Link02-2_joint"
nameJoint[3]	= "Link03-2_joint"
nameJoint[4]	= "Link04-5_joint"
nameJoint[5]	= "Link05-3_joint"
nameJoint[6]	= "Link06-4_joint"
nameJoint[7]	= "Link07-2_joint"

-- Joint devices
removeAllDevices(nameBody[1])

-- Acatuator
nameDevice[1]	= addDevice(nameJoint[1], "Joint", "devices\\motor_v1.0.dml")
nameDevice[2]	= addDevice(nameJoint[2], "Joint", "devices\\motor_v1.0.dml")
nameDevice[3]	= addDevice(nameJoint[3], "Joint", "devices\\motor_v1.0.dml")
nameDevice[4]	= addDevice(nameJoint[4], "Joint", "devices\\motor_v1.0.dml")
nameDevice[5]	= addDevice(nameJoint[5], "Joint", "devices\\motor_v1.0.dml")
nameDevice[6]	= addDevice(nameJoint[6], "Joint", "devices\\motor_v1.0.dml")
nameDevice[7]	= addDevice(nameJoint[7], "Joint", "devices\\motor_v1.0.dml")
nameDevice[8]	= addDevice(nameBody[1], "Body", "devices\\wrench_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "motor1")
setStringProperty(nameDevice[2], "Device", "Name", "motor2")
setStringProperty(nameDevice[3], "Device", "Name", "motor3")
setStringProperty(nameDevice[4], "Device", "Name", "motor4")
setStringProperty(nameDevice[5], "Device", "Name", "motor5")
setStringProperty(nameDevice[6], "Device", "Name", "motor6")
setStringProperty(nameDevice[7], "Device", "Name", "motor7")
setStringProperty(nameDevice[8], "Device", "Name", "wrench")

-- Encoder
nameDevice[1]	= addDevice(nameJoint[1], "Joint", "devices\\enc_v1.0.dml")
nameDevice[2]	= addDevice(nameJoint[2], "Joint", "devices\\enc_v1.0.dml")
nameDevice[3]	= addDevice(nameJoint[3], "Joint", "devices\\enc_v1.0.dml")
nameDevice[4]	= addDevice(nameJoint[4], "Joint", "devices\\enc_v1.0.dml")
nameDevice[5]	= addDevice(nameJoint[5], "Joint", "devices\\enc_v1.0.dml")
nameDevice[6]	= addDevice(nameJoint[6], "Joint", "devices\\enc_v1.0.dml")
nameDevice[7]	= addDevice(nameJoint[7], "Joint", "devices\\enc_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "enc1")
setStringProperty(nameDevice[2], "Device", "Name", "enc2")
setStringProperty(nameDevice[3], "Device", "Name", "enc3")
setStringProperty(nameDevice[4], "Device", "Name", "enc4")
setStringProperty(nameDevice[5], "Device", "Name", "enc5")
setStringProperty(nameDevice[6], "Device", "Name", "enc6")
setStringProperty(nameDevice[7], "Device", "Name", "enc7")

-- Tachometer
nameDevice[1]	= addDevice(nameJoint[1], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[2]	= addDevice(nameJoint[2], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[3]	= addDevice(nameJoint[3], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[4]	= addDevice(nameJoint[4], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[5]	= addDevice(nameJoint[5], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[6]	= addDevice(nameJoint[6], "Joint", "devices\\tacho_v1.0.dml")
nameDevice[7]	= addDevice(nameJoint[7], "Joint", "devices\\tacho_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "tacho1")
setStringProperty(nameDevice[2], "Device", "Name", "tacho2")
setStringProperty(nameDevice[3], "Device", "Name", "tacho3")
setStringProperty(nameDevice[4], "Device", "Name", "tacho4")
setStringProperty(nameDevice[5], "Device", "Name", "tacho5")
setStringProperty(nameDevice[6], "Device", "Name", "tacho6")
setStringProperty(nameDevice[7], "Device", "Name", "tacho7")


-- Sensor
nameDevice[1]	= addDevice(nameBody[1], "Body", "devices\\htransform_v1.0.dml")
nameDevice[2]	= addDevice(nameBody[1], "Body", "devices\\twist_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "htransform")
setStringProperty(nameDevice[2], "Device", "Name", "twist")

-- Shared memory
removeDevice("sm_data")
nameDevice[1]	= addDevice("Subsys", "Subsys", "devices\\shared_memory_v1.0.dml")
setStringProperty(nameDevice[1], "Device", "Name", "sm_data")