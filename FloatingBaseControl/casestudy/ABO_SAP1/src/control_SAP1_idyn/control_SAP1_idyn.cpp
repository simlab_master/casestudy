/* RoboticsLab, Copyright 2008-2010 SimLab Co., Ltd. All rights reserved.
 *
 * This library is commercial and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF SimLab Co., LTD.
 */
#include "control_SAP1_idyn.h"
#include "control_SAP1_idynCmd.h"
#include "rConst.h"

control_SAP1_idyn::control_SAP1_idyn(rDC rdc) 
#ifdef _USE_RCONTROLALGORITHM_EX_
:rControlAlgorithmEx(rdc)
#else
:rControlAlgorithm(rdc)
#endif
, _sys(NULL), _controlSet(NULL), _home(NULL), _ht_ctrl(NULL)
, _dT(0)
, _jdof(0)
, _bApplyWrench(false)
{
	_HT_target.R.identity();
    _HT_target.r.zero();
    _HT_time_due = 1.0;

	for(int i = 0; i < MAX_JDOF; i ++)
	{
		_hdev_motor[i]	= INVALID_RHANDLE;
		_hdev_enc[i]	= INVALID_RHANDLE;
		_hdev_tacho[i]	= INVALID_RHANDLE;

		_buf_q[i]		= 0;
		_buf_qdot[i]	= 0;
		_buf_torque[i]	= 0;
	}

	_hdev_wrench		= INVALID_RHANDLE;
	_hdev_htransform	= INVALID_RHANDLE;
	_hdev_twist			= INVALID_RHANDLE;

	for(int i = 0; i < 12; i++)
		_buf_htransform[i] = 0;

	for(int i = 0; i < 6; i++)
	{
		_buf_twist[i]	= 0;
		_buf_wrench[i]	= 0;
	}

	_hdev_sm_data		= INVALID_RHANDLE;

	for(int i = 0; i < 13; i++)
		_buf_e[i] = 0;
}

control_SAP1_idyn::~control_SAP1_idyn()
{
	removeInterestFrame(_T("IF_tg"));
	removeInterestFrame(_T("IF_td"));

	delete _controlSet;

	FREE_SYSTEMS();	
}

void control_SAP1_idyn::_arrangeJointDevices()
{
	for (int i = 0; i < _jdof; i++)
	{
		TCHAR devname[32];
		_stprintf_s(devname, _T("motor%d"), i + 1);
		_hdev_motor[i] = findDevice(devname);

		_stprintf_s(devname, _T("enc%d"), i + 1);
		_hdev_enc[i] = findDevice(devname);

		_stprintf_s(devname, _T("tacho%d"), i + 1);
		_hdev_tacho[i] = findDevice(devname);
	}

	_hdev_wrench		= findDevice(_T("wrench"));
	_hdev_htransform	= findDevice(_T("htransform"));
	_hdev_twist			= findDevice(_T("twist"));

	_hdev_sm_data		= findDevice(_T("sm_data"));
}

void control_SAP1_idyn::init(int mode)
{
	// construct a nominal system
	_sys = LOAD_SYSTEM(_path, _aml, _T0, _q0);
	assert(_sys);

	_jdof = _sys->jointDOF() + _sys->earthDOF() + _sys->constraintDOF();

	_q.resize(_jdof);
	_qdot.resize(_jdof);
	_torque.resize(_jdof);
	_torque.zero();

	_arrangeJointDevices();

	_setControllers(mode);

	_q.zero();
	_qdot.zero();
	_torque.zero();

	addInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.3, 0.0, 0.0)));
	addInterestFrame(_T("IF_td"), HTransform(Rotation(), Displacement(0.3, 0.0, 0.0)));
}

void control_SAP1_idyn::_setControllers(int mode)
{
	// Before setting controllers, systems reflect current states.
	_readDevices();
	_estimate();
	_reflect();
	
	// construct the control set
	_controlSet = new rxControlSet(_sys, _dT, false, true);
	_controlSet->setGravity(0, 0, -GRAV_ACC);
	
	// home controller
	_home = new rxInterpolatedJointController(_sys, _dT);
	_home->setGain(200.0, 200.0, 1.0);
	_home->deactivate();
	_controlSet->addController(_home, _T("home_ctrl"));

	// task controller
	rxBody* base = _sys->getUCSBody(_T("Base"), _HT_base_ref);	// reference body
	
	HTransform T_wrist_target;									// target frame
	rxBody* wrist = _sys->findBody(_T("Link07-2"));				// target body	

	_ht_ctrl = new rxInterpolatedHTransformController(_sys, wrist, T_wrist_target, base, _HT_base_ref, _dT);
	_ht_ctrl->setGain(20.0, 200.0, 0.0);
	_ht_ctrl->deactivate();
	_controlSet->addController(_ht_ctrl, _T("ht_ctrl"));

	HTransform hT;

	// Interpolated displacement compliance control
	_idc_control = new rxInterpolatedDisplacementComplianceController(_sys, base, hT.r, NULL, hT.r, _dT);
	_idc_control->setGain(20, 200);
	_idc_control->deactivate();
	_controlSet->addController(_idc_control, _T("idc_ctrl"));

	// Interpolated Orientation compliance control
	_ioc_control = new rxInterpolatedOrientationComplianceController(_sys, base, hT.R,  NULL, hT.R, _dT);
	_ioc_control->setGain(20, 200);
	_ioc_control->deactivate();
	_controlSet->addController(_ioc_control, _T("ioc_ctrl"));

	_controlSet->nullMotionController()->setGain(10.0, 0.0, 0.0);
}

void control_SAP1_idyn::update(const rTime& t)
{
	rControlAlgorithm::update(t);
}

void control_SAP1_idyn::setNominalSystem(const TCHAR* path, const TCHAR* aml, const HTransform& T0, const dVector& q0)
{
	_path = path;
	_aml = aml;
	_T0 = T0;
	_q0 = q0;
}

void control_SAP1_idyn::setPeriod(const rTime& dT)
{
	_dT = dT;
}

void control_SAP1_idyn::_readDevices()
{
	for(int i = 0; i < _jdof; i++)
	{
		if(_hdev_enc[i] != INVALID_RHANDLE)
		{
			readDeviceValue(_hdev_enc[i], &_buf_q[i], sizeof(float));
			_q[i] = _buf_q[i];
		}

		if(_hdev_tacho[i] != INVALID_RHANDLE)
		{
			readDeviceValue(_hdev_tacho[i], &_buf_qdot[i], sizeof(float));
			_qdot[i] = _buf_qdot[i];
		}
	}

	if(_hdev_htransform != INVALID_RHANDLE)
	{
		readDeviceValue(_hdev_htransform, _buf_htransform, sizeof(_buf_htransform));
		
		_t.R = Rotation(
			_buf_htransform[0], _buf_htransform[1], _buf_htransform[2],
			_buf_htransform[3], _buf_htransform[4], _buf_htransform[5],
			_buf_htransform[6], _buf_htransform[7], _buf_htransform[8]
		);
		_t.r[0]	= _buf_htransform[9];
		_t.r[1]	= _buf_htransform[10];
		_t.r[2]	= _buf_htransform[11];
	}

	if(_hdev_htransform != INVALID_RHANDLE)
	{
		readDeviceValue(_hdev_twist, _buf_twist, sizeof(_buf_twist));

		_v = Twist(
			Vector3D(_buf_twist[0], _buf_twist[1], _buf_twist[2]),
			Vector3D(_buf_twist[3], _buf_twist[4], _buf_twist[5])
			);
	}
}

void control_SAP1_idyn::_writeDevices()
{
	int res = 0;
	for(int i = 0; i < _jdof; i++)
	{
		if(_hdev_motor[i] != INVALID_RHANDLE)
		{
			_buf_torque[i] = (float)_torque[6 + i];
			res = writeDeviceValue(_hdev_motor[i], &_buf_torque[i], sizeof(float));
		}
	}

	if(_hdev_wrench != INVALID_RHANDLE && _bApplyWrench)
	{
		for(int i = 0; i < 6; i++)
			_buf_wrench[i] = (float)_torque[i];
		res = writeDeviceValue(_hdev_wrench, _buf_wrench, sizeof(float) * 6);
	}

	if(_hdev_sm_data != INVALID_RHANDLE)
	{
		for(int i = 0; i < 7; i++)
		{
			if(_home)
				_buf_sm_data[i] = (float)_home->qd()[i];
			else
				_buf_sm_data[i] = 0.0f;
		}
		
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				_buf_sm_data[7 + i* 3 + j]		= (float)_sys->T().R(i, j);
		
		for(int i = 0; i < 3; i++)
			_buf_sm_data[16 + i]				= (float)_sys->T().r[i];

		for(int i = 0; i < 3; i++)
			_buf_sm_data[19 + i]				= (float)_sys->V().v[i];

		for(int i = 0; i < 3; i++)
			_buf_sm_data[22 + i]				= (float)_sys->V().w[i];
		
		int res = writeDeviceValue(_hdev_sm_data, _buf_sm_data, sizeof(_buf_sm_data));
	}
}

void control_SAP1_idyn::_reflect()
{
	_sys->q(_q);
	_sys->qdot(_qdot);
	_sys->T(_t);
	_sys->V(_v);
}

void control_SAP1_idyn::_compute(const double& t)
{
	if (_controlSet)
	{
		_controlSet->compute(t, _torque);
		for(unsigned int i = 0; i < _controlSet->e().size(); i++)
			_buf_e[i] = _controlSet->e()[i];
	}
}

void control_SAP1_idyn::_estimate()
{
}

int control_SAP1_idyn::command(const short& cmd, const int& arg)
{
	switch (cmd)
	{
	case ACTIVATE_HOME_CTRL:		
		{
			if (!_idc_control->activated())
				_idc_control->activate();

			if (!_ioc_control->activated())
				_ioc_control->activate();

            if (_ht_ctrl->activated())
				_ht_ctrl->deactivate();
			
			assert(_home);
			_home->activate();
			
			// add the next target point during two seconds. 
			dVector q_home(_sys->jointDOF());
			q_home.zero();

			q_home[1] = 30*DEGREE;
			q_home[3] = 30*DEGREE;
			q_home[5] = 30*DEGREE;

			_home->addPoint(q_home, 1);
		}

		break;

	case DEACTIVATE_HOME_CTRL:		
		_home->deactivate();

		break;

	case ACTIVATE_HT_CTRL:		
		{
			if (!_idc_control->activated())
				_idc_control->activate();

			if (!_ioc_control->activated())
				_ioc_control->activate();

            if (_home->activated())
				_home->deactivate();

			assert(_ht_ctrl);
			_ht_ctrl->activate();

			_ht_ctrl->addPoint(_HT_target, _HT_time_due);
		}

		break;

	case DEACTIVATE_HT_CTRL:
		_ht_ctrl->deactivate();
		break;

	case SET_J_TARGET_PRE_DEF_01:
		{
			if (_ht_ctrl->activated())
				_ht_ctrl->deactivate();

			assert(_home);
			_home->activate();
			
			dVector q_home(_sys->jointDOF());
			q_home.zero();

			//_home->addPoint(q_home, 1);
			_home->addPoint(_q, 1);
		}
		break;

	case SET_HT_TARGET_PRE_DEF_01:
		{
			_HT_target.R.identity();
			_HT_target.R.YRotate(90*DEGREE);
			_HT_target.r.zero();
			_HT_target.r[0] = 0.3;
			_HT_target.r[2] = 0.3;
			setInterestFrame(_T("IF_tg"), _HT_target);
		}
		break;

	case SET_HT_TARGET_PRE_DEF_02:
		{
			_HT_target.R.identity();
			_HT_target.r.zero();
			_HT_target.r[0] = 0.1;
			_HT_target.r[2] = 0.5;
			setInterestFrame(_T("IF_tg"), _HT_target);
		}
		break;

	case SET_HT_TARGET_PRE_DEF_03:
		{
			_HT_target.R.identity();
			_HT_target.R.YRotate(135*DEGREE);
			_HT_target.R.XRotate(45*DEGREE);
			_HT_target.r.zero();
			_HT_target.r[0] = 0.2;
			_HT_target.r[1] = -0.2;
			_HT_target.r[2] = 0.2;
			setInterestFrame(_T("IF_tg"), _HT_target);
		}
		break;

	case SET_HT_TARGET_INC_X:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.1, 0.0, 0.0)));
		}
		break;

	case SET_HT_TARGET_INC_Y:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.0, 0.1, 0.0)));
		}
		break;

	case SET_HT_TARGET_INC_Z:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.0, 0.0, 0.1)));
		}
		break;

	case SET_HT_TARGET_DEC_X:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(-0.1, 0.0, 0.0)));
		}
		break;

	case SET_HT_TARGET_DEC_Y:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.0, -0.1, 0.0)));
		}
		break;

	case SET_HT_TARGET_DEC_Z:
		{
			incrementInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.0, 0.0, -0.1)));
		}
		break;

	case SET_WRENCH:
		_bApplyWrench = !_bApplyWrench;
		break;

	default:
		break;
	}

	return 0;
}

void control_SAP1_idyn::datanames(vector<string_type>& names, int channel)
{
	if(channel == 0)
	{
		names.push_back(_T("q1"));
		names.push_back(_T("q2"));
		names.push_back(_T("q3"));
		names.push_back(_T("q4"));
		names.push_back(_T("q5"));
		names.push_back(_T("q6"));
		names.push_back(_T("q7"));

		names.push_back(_T("qd1"));
		names.push_back(_T("qd2"));
		names.push_back(_T("qd3"));
		names.push_back(_T("qd4"));
		names.push_back(_T("qd5"));
		names.push_back(_T("qd6"));
		names.push_back(_T("qd7"));

		names.push_back(_T("torque1(7)"));
		names.push_back(_T("torque2(8)"));
		names.push_back(_T("torque3(9)"));
		names.push_back(_T("torque4(10)"));
		names.push_back(_T("torque5(11)"));
		names.push_back(_T("torque6(12)"));
		names.push_back(_T("torque7(13)"));
	}
	else if(channel == 1)
	{
		names.push_back(_T("tx"));
		names.push_back(_T("ty"));
		names.push_back(_T("tz"));

		names.push_back(_T("lvx"));
		names.push_back(_T("lvy"));
		names.push_back(_T("lvz"));
		names.push_back(_T("avx"));
		names.push_back(_T("avy"));
		names.push_back(_T("avz"));
	}
	else if(channel == 2)
	{
		names.push_back(_T("torque1(tx)"));
		names.push_back(_T("torque2(ty)"));
		names.push_back(_T("torque3(tz)"));
		names.push_back(_T("torque4(mx)"));
		names.push_back(_T("torque5(my)"));
		names.push_back(_T("torque6(mz)"));
	}
	else if(channel == 3)
	{
		names.push_back(_T("error1"));
		names.push_back(_T("error2"));
		names.push_back(_T("error3"));
		names.push_back(_T("error4"));
		names.push_back(_T("error5"));
		names.push_back(_T("error6"));
		names.push_back(_T("error7"));
		names.push_back(_T("error8"));
		names.push_back(_T("error9"));
		names.push_back(_T("error10"));
		names.push_back(_T("error11"));
		names.push_back(_T("error12"));
		names.push_back(_T("error13"));
	}
}

void control_SAP1_idyn::collect(vector<double>& data, int channel)
{
	if(channel == 0)
	{
		for(int i = 0; i < _jdof; i++)
			data.push_back(_q[i]);

		for(int i = 0; i < _jdof; i++)
		{
			if(_home)
				data.push_back(_home->qd()[i]);
			else
				data.push_back(0);
		}

		for(int i = 6; i < _jdof + 6; i++)
			data.push_back(_torque[i]);
	}
	else if(channel == 1)
	{
		for(int i = 9; i < 12; i++)
			data.push_back(_buf_htransform[i]);

		for(int i = 0; i < 6; i++)
			data.push_back(_buf_twist[i]);
	}
	else if(channel == 2)
	{
		for(int i = 0; i < 6; i++)
			data.push_back(_torque[i]);
	}
	else if(channel == 3)
	{
		for(int i = 0; i < 13; i++)
			data.push_back(_buf_e[i]);
	}
}

void control_SAP1_idyn::onSetInterestFrame(const TCHAR* name, const HTransform& T)
{
	command(DEACTIVATE_HOME_CTRL);
	_HT_target = T * _t.Inverse();
	command(ACTIVATE_HT_CTRL);
}

rControlAlgorithm* CreateControlAlgorithm(rDC& rdc)
{
	return new control_SAP1_idyn(rdc);
}
