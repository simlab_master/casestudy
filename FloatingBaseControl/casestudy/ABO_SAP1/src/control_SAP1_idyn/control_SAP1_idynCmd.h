/* RoboticsLab, Copyright 2008-2010 SimLab Co., Ltd. All rights reserved.
 *
 * This library is commercial and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF SimLab Co., LTD.
 */
#ifndef __CONTROL_SAP1_IDYN_CMD_H__
#define __CONTROL_SAP1_IDYN_CMD_H__

#include "rCommand/rCmdDefine.h"

#define ACTIVATE_HOME_CTRL			(RCMD_USER + 0x0a)
#define DEACTIVATE_HOME_CTRL		(RCMD_USER + 0x0b)
#define ACTIVATE_HT_CTRL			(RCMD_USER + 0x0c)
#define DEACTIVATE_HT_CTRL			(RCMD_USER + 0x0d)

#define SET_HT_TARGET_PRE_DEF_01	(RCMD_USER + 0x11)
#define SET_HT_TARGET_PRE_DEF_02	(RCMD_USER + 0x12)
#define SET_HT_TARGET_PRE_DEF_03	(RCMD_USER + 0x13)
#define SET_HT_TARGET_INC_X	        (RCMD_USER + 0x14)
#define SET_HT_TARGET_INC_Y	        (RCMD_USER + 0x15)
#define SET_HT_TARGET_INC_Z	        (RCMD_USER + 0x16)
#define SET_HT_TARGET_DEC_X	        (RCMD_USER + 0x17)
#define SET_HT_TARGET_DEC_Y	        (RCMD_USER + 0x18)
#define SET_HT_TARGET_DEC_Z	        (RCMD_USER + 0x19)

#define SET_J_TARGET_PRE_DEF_01		(RCMD_USER + 0x21)

#define SET_WRENCH					(RCMD_USER + 0x31)

#endif