local nameDevice	= {}
local nameJoint		= {}
local nameBody		= {}

-- Set Body Name
nameBody[1]			= "Base"

removeAllDevices(nameBody[1])

-- Acatuator
nameDevice[1]	= addDevice(nameBody[1], "Body", "devices\\wrench_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "wrench")

-- Sensor
nameDevice[1]	= addDevice(nameBody[1], "Body", "devices\\htransform_v1.0.dml")
nameDevice[2]	= addDevice(nameBody[1], "Body", "devices\\twist_v1.0.dml")

setStringProperty(nameDevice[1], "Device", "Name", "htransform")
setStringProperty(nameDevice[2], "Device", "Name", "twist")

-- Shared memory
removeDevice("sm_data")
nameDevice[1]	= addDevice("Subsys", "Subsys", "devices\\shared_memory_v1.0.dml")
setStringProperty(nameDevice[1], "Device", "Name", "sm_data")