/* RoboticsLab, Copyright 2008-2011 SimLab Co., Ltd. All rights reserved.
 *
 * This library is commercial and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF SimLab Co., LTD.
 */
#include "stdafx.h"

#include "rMath/rMath.h"
using namespace rMath;

#include "../control_SimpleFloatingBox_wrench/control_SimpleFloatingBox_wrenchCmd.h"
#include "rxSDK/rxSDK.h"
#include "rCommon/rCodeUtil.h"

bool				bContact		= true;		// Enables/disables contact dynamics.
bool				bQuit			= false;	// Set this flag true to quit this program.
bool				bRun			= false;	// Set this flag true to activate the program.

const rTime			delT			= 0.005;
string_type			aml_path		= _T("models/Test/SimpleFloatingBox/SimpleFloatingBox.aml");
string_type			aml_name		= _T("ABO_SAP1");
string_type			aml_path_nm		= _T("models/Test/SimpleFloatingBox/SimpleFloatingBox.aml");
string_type			aml_name_nm		= _T("ABO_SAP1_nominal");

HTransform			aml_T0;
dVector				aml_q0;
rxSystem			*sys			= NULL;
rxSystem			*sys_nominal	= NULL;

string_type			control_path	= _T("controls/control_SimpleFloatingBox_wrench.dll");
string_type			control_name	= _T("control_SimpleFloatingBox_wrench");
rxControlInterface	*control		= NULL;

rxDevice			*sm_data		= NULL;
float				buf_sm_data[25];
dVector				nq;
HTransform			nt;
Twist				nv;

double force		= 88.0;
rxBody *pBody		= NULL;
rxDevice *pDevFT	= NULL;

void MyKeyboardHandler(int key, void* data);
void MyControlCallback(rTime time, void* data);

int _tmain(int argc, _TCHAR* argv[])
{
	rCreateWorld(bContact, delT);
	rSetGravity(0, 0, -GRAV_ACC);
	rCreatePlane(0, 0, 1, 0);

	aml_T0.r[0]	= 0.0;
	aml_T0.r[1] = 0.0;
	aml_T0.r[2] = 1.0;
	//aml_T0.R.YRotate(90*DEGREE);

	aml_q0.resize(7);
	aml_q0.zero();
	//aml_q0.all(30 * DEGREE);
	aml_q0[3] = 1.57;
	
	sys				= rCreateSystem(aml_path, aml_name, aml_T0, aml_q0);
	//sys_nominal		= rCreateStaticSystem(aml_path_nm, aml_name_nm, aml_T0, aml_q0);
	
	rInitializeEx(true, true);

	if (sys)
	{
		int step = 1;
		control = rCreateController(control_name, sys, step);
		control->setAlgorithmDll(control_path);
		control->setPeriod(step*delT);
		control->setNominalSystem(aml_path, aml_name, aml_T0, aml_q0);
		control->initAlgorithm();
	}

	sm_data = sys->findDevice(_T("sm_data"));
	nq.resize(7);

	pBody	= sys->findBody(_T("Base"));
	pDevFT	= sys->findDevice(_T("ft"));

	rAddKeyboardHandler(MyKeyboardHandler, NULL);
	rAddControlHandler(MyControlCallback, NULL);

	rID plot_tv	= rdaqCreatePlot(_T("plot_tv"), eDataPlotType_TimeLine);
	rdaqAddData(plot_tv, control, 1);

	rID plot_bt	= rdaqCreatePlot(_T("plot_bt"), eDataPlotType_TimeLine);
	rdaqAddData(plot_bt, control, 2);

	if(pDevFT)
	{
		rID plot_ft = rdaqCreatePlot(_T("ft"), eDataPlotType_TimeLine);
		rdaqAddData(plot_ft, pDevFT, eDeviceDataType_ReadFloat);
	}
	
	rRun(0);

	return 0;
}

void MyKeyboardHandler(int key, void* data)
{
	switch (key)
	{
	case VK_TAB:
		{
			bRun = !bRun;
			if(bRun)
				rActivateWorld();
			else
				rDeactivateWorld();
		}
		break;
	
	case VK_Q:
		{
			rDeactivateWorld();
			rQuit();
		}
		break;

	case VK_H:
		{
			if(control)
				control->command(ACTIVATE_HOME_CTRL);
		}
		break;

	case VK_1:
		if(control)
			control->command(SET_HT_TARGET_PRE_DEF_01);
		break;

	case VK_2:
		if(control)
			control->command(SET_HT_TARGET_PRE_DEF_02);
		break;

	case VK_3:
		if(control)
			control->command(SET_HT_TARGET_PRE_DEF_03);
		break;

	case VK_W:
		if(control)
			control->command(SET_HT_TARGET_INC_X);
		break;

	case VK_S:
		if(control)
			control->command(SET_HT_TARGET_DEC_X);
		break;

	case VK_E:
		if(control)
			control->command(SET_HT_TARGET_INC_Y);
		break;

	case VK_D:
		if(control)
			control->command(SET_HT_TARGET_DEC_Y);
		break;

	case VK_R:
		if(control)
			control->command(SET_HT_TARGET_INC_Z);
		break;

	case VK_F:
		if(control)
			control->command(SET_HT_TARGET_DEC_Z);
		break;
		
	case VK_P:
		if(control)
			control->command(SET_WRENCH);
		break;

	case VK_UP:
		force += 10;
		printf("Force : %f\n", force);
		break;

	case VK_DOWN:
		force -= 10;
		printf("Force : %f\n", force);
		break;
	}
}

void MyControlCallback(rTime time, void* data)
{
	/*if(pBody)
		pBody->applyWrench(Vector3D(0, 0, force), Vector3D());*/

	if(sm_data)
	{
		int res = sm_data->readDeviceValue(buf_sm_data, sizeof(buf_sm_data));

		nt.R = Rotation(
			buf_sm_data[7], buf_sm_data[8], buf_sm_data[9],
			buf_sm_data[10], buf_sm_data[11], buf_sm_data[12],
			buf_sm_data[13], buf_sm_data[14], buf_sm_data[15]
		);
		nt.r[0]	= buf_sm_data[16];
		nt.r[1]	= buf_sm_data[17];
		nt.r[2]	= buf_sm_data[18];

		nv = Twist(
			Vector3D(buf_sm_data[19], buf_sm_data[20], buf_sm_data[21]),
			Vector3D(buf_sm_data[22], buf_sm_data[23], buf_sm_data[24])
			);

		if(sys_nominal)
		{
			sys_nominal->T(nt);
			sys_nominal->V(nv);
		}
	}
}
