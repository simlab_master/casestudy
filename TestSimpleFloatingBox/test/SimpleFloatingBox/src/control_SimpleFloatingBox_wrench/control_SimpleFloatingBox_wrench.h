/* RoboticsLab, Copyright 2008-2010 SimLab Co., Ltd. All rights reserved.
 *
 * This library is commercial and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF SimLab Co., LTD.
 */
#ifndef __CONTROL_SAP1_IDYN_H__
#define __CONTROL_SAP1_IDYN_H__

#include <list>
#include "rControlAlgorithm/rControlAlgorithm.h"
#include "rxControlSDK/rxControlSDK.h"

//#define _USE_RCONTROLALGORITHM_EX_

#define MAX_JDOF	20

#ifdef _USE_RCONTROLALGORITHM_EX_
class REXPORT control_SimpleFloatingBox_wrench : public rControlAlgorithmEx
#else
class REXPORT control_SimpleFloatingBox_wrench : public rControlAlgorithm
#endif
{
public:
	control_SimpleFloatingBox_wrench(rDC rdc);
	~control_SimpleFloatingBox_wrench();

	virtual void init(int mode = 0);
	virtual void update(const rTime& t);
	virtual void setNominalSystem(const TCHAR* path, const TCHAR* aml, const HTransform& T0, const dVector& q0);
	virtual void setPeriod(const rTime& dT);
	virtual int command(const short& cmd, const int& arg = 0);
	virtual void datanames(vector<string_type>& names, int channel = -1);
	virtual void collect(vector<double>& data, int channel = -1);
	virtual void onSetInterestFrame(const TCHAR* name, const HTransform& T);

private:
	virtual void _estimate();
	virtual void _readDevices();
	virtual void _writeDevices();

	virtual void _reflect();
	virtual void _compute(const rTime& t);

	void _arrangeDevices();

	void _setControllers(int mode = 0); 

private:

	dVector				_torque;
	HTransform			_t;
	Twist				_v;

	rHANDLE				_hdev_wrench;
	rHANDLE				_hdev_htransform;
	rHANDLE				_hdev_twist;

	float				_buf_htransform[12];
	float				_buf_twist[6];
	float				_buf_wrench[6];

	rxSystem							*_sys;
	rxControlSetBase					*_controlSet;
	rxDisplacementComplianceController	*_idc_control;
	rxOrientationComplianceController	*_ioc_control;
	
	string_type			_path;
	string_type			_aml;
	HTransform			_T0;
	dVector				_q0;

	double				_dT;

	bool				_bApplyWrench;

	HTransform			_HT_base_ref;
	Vector3D			_goal_disp;
	Rotation			_goal_rot;

	rHANDLE				_hdev_sm_data;
	float				_buf_sm_data[25];
};
#endif