/* RoboticsLab, Copyright 2008-2010 SimLab Co., Ltd. All rights reserved.
 *
 * This library is commercial and cannot be redistributed, and/or modified
 * WITHOUT ANY ALLOWANCE OR PERMISSION OF SimLab Co., LTD.
 */
#include "control_SimpleFloatingBox_wrench.h"
#include "control_SimpleFloatingBox_wrenchCmd.h"
#include "rConst.h"

control_SimpleFloatingBox_wrench::control_SimpleFloatingBox_wrench(rDC rdc) 
#ifdef _USE_RCONTROLALGORITHM_EX_
:rControlAlgorithmEx(rdc)
#else
:rControlAlgorithm(rdc)
#endif
, _sys(NULL), _controlSet(NULL)
, _idc_control(NULL)
, _ioc_control(NULL)
, _dT(0)
, _bApplyWrench(false)
{
	_hdev_wrench		= INVALID_RHANDLE;
	_hdev_htransform	= INVALID_RHANDLE;
	_hdev_twist			= INVALID_RHANDLE;

	_hdev_sm_data		= INVALID_RHANDLE;
}

control_SimpleFloatingBox_wrench::~control_SimpleFloatingBox_wrench()
{
	removeInterestFrame(_T("IF_tg"));
	removeInterestFrame(_T("IF_td"));

	delete _controlSet;

	FREE_SYSTEMS();	
}

void control_SimpleFloatingBox_wrench::_arrangeDevices()
{
	_hdev_wrench		= findDevice(_T("wrench"));
	_hdev_htransform	= findDevice(_T("htransform"));
	_hdev_twist			= findDevice(_T("twist"));

	_hdev_sm_data		= findDevice(_T("sm_data"));
}

void control_SimpleFloatingBox_wrench::init(int mode)
{
	// construct a nominal system
	_sys = LOAD_SYSTEM(_path, _aml, _T0, _q0);
	assert(_sys);

	_torque.resize(6);
	_torque.zero();

	_arrangeDevices();

	_setControllers(mode);

	addInterestFrame(_T("IF_tg"), HTransform(Rotation(), Displacement(0.3, 0.0, 0.0)));
	addInterestFrame(_T("IF_td"), HTransform(Rotation(), Displacement(0.3, 0.0, 0.0)));
}

void control_SimpleFloatingBox_wrench::_setControllers(int mode)
{
	// Before setting controllers, systems reflect current states.
	_readDevices();
	_estimate();
	_reflect();
	
	// construct the control set
	_controlSet = new rxControlSet(_sys, _dT, false, true);
	//_controlSet = new rxComplianceControlSet(_sys, _dT, false, true);
	_controlSet->setGravity(0, 0, -GRAV_ACC);
	
	rxBody* base = _sys->getUCSBody(_T("Base"), _HT_base_ref);	// reference body
	HTransform hT;

	double scale_gain = 2.0;
	double kd = 9.0*0.001/_dT * scale_gain;
	double kp = kd*kd*5.0;

	// Interpolated displacement compliance control
	_idc_control = new rxInterpolatedDisplacementComplianceController(_sys, base, hT.r, NULL, hT.r, _dT);
	_idc_control->setStiffness(0, kd, kp);
	_idc_control->setStiffness(1, kd, kp);
	_idc_control->setStiffness(2, kd, kp);
	//_idc_control->setGain(20, 200);
	_idc_control->deactivate();
	_controlSet->addController(_idc_control, _T("idc_ctrl"));

	// Interpolated Orientation compliance control
	_ioc_control = new rxInterpolatedOrientationComplianceController(_sys, base, hT.R,  NULL, hT.R, _dT);
	_ioc_control->setStiffness(0, kd, kp);
	_ioc_control->setStiffness(1, kd, kp);
	_ioc_control->setStiffness(2, kd, kp);
	//_ioc_control->setGain(20, 200);
	_ioc_control->deactivate();
	_controlSet->addController(_ioc_control, _T("ioc_ctrl"));

	//_controlSet->nullMotionController()->setGain(10.0, 0.0, 0.0);	
	_controlSet->setInverseDynamicsAlgorithm(new rxAMBSGravCompensation(_sys));
	_controlSet->setNullMotionController(NULL);
}

void control_SimpleFloatingBox_wrench::update(const rTime& t)
{
	rControlAlgorithm::update(t);
}

void control_SimpleFloatingBox_wrench::setNominalSystem(const TCHAR* path, const TCHAR* aml, const HTransform& T0, const dVector& q0)
{
	_path = path;
	_aml = aml;
	_T0 = T0;
	_q0 = q0;
}

void control_SimpleFloatingBox_wrench::setPeriod(const rTime& dT)
{
	_dT = dT;
}

void control_SimpleFloatingBox_wrench::_readDevices()
{
	if(_hdev_htransform != INVALID_RHANDLE)
	{
		readDeviceValue(_hdev_htransform, _buf_htransform, sizeof(_buf_htransform));
		
		_t.R = Rotation(
			_buf_htransform[0], _buf_htransform[1], _buf_htransform[2],
			_buf_htransform[3], _buf_htransform[4], _buf_htransform[5],
			_buf_htransform[6], _buf_htransform[7], _buf_htransform[8]
		);
		_t.r[0]	= _buf_htransform[9];
		_t.r[1]	= _buf_htransform[10];
		_t.r[2]	= _buf_htransform[11];
	}

	if(_hdev_htransform != INVALID_RHANDLE)
	{
		readDeviceValue(_hdev_twist, _buf_twist, sizeof(_buf_twist));

		_v = Twist(
			Vector3D(_buf_twist[0], _buf_twist[1], _buf_twist[2]),
			Vector3D(_buf_twist[3], _buf_twist[4], _buf_twist[5])
			);
	}
}

void control_SimpleFloatingBox_wrench::_writeDevices()
{
	int res = 0;
	
	if(_hdev_wrench != INVALID_RHANDLE && _bApplyWrench)
	{
		for(int i = 0; i < 6; i++)
			_buf_wrench[i] = (float)_torque[i];
		res = writeDeviceValue(_hdev_wrench, _buf_wrench, sizeof(float) * 6);
	}

	if(_hdev_sm_data != INVALID_RHANDLE)
	{
		for(int i = 0; i < 3; i++)
			for(int j = 0; j < 3; j++)
				_buf_sm_data[7 + i* 3 + j]		= (float)_sys->T().R(i, j);
		
		for(int i = 0; i < 3; i++)
			_buf_sm_data[16 + i]				= (float)_sys->T().r[i];

		for(int i = 0; i < 3; i++)
			_buf_sm_data[19 + i]				= (float)_sys->V().v[i];

		for(int i = 0; i < 3; i++)
			_buf_sm_data[22 + i]				= (float)_sys->V().w[i];
		
		int res = writeDeviceValue(_hdev_sm_data, _buf_sm_data, sizeof(_buf_sm_data));
	}
}

void control_SimpleFloatingBox_wrench::_reflect()
{
	_sys->T(_t);
	_sys->V(_v);
}

void control_SimpleFloatingBox_wrench::_compute(const double& t)
{
	if (_controlSet)
	{
		_controlSet->compute(t, _torque);
		//_controlSet->e();
	}
}

void control_SimpleFloatingBox_wrench::_estimate()
{
}

int control_SimpleFloatingBox_wrench::command(const short& cmd, const int& arg)
{
	switch (cmd)
	{
	case ACTIVATE_HOME_CTRL:		
		{
			if (!_idc_control->activated())
				_idc_control->activate();
			
			if (!_ioc_control->activated())
				_ioc_control->activate();

			_goal_disp.zero();
			for(int i = 0; i < 3; i++)
				_goal_disp[i]	= _T0.r[i];
			_goal_rot = _T0.R;

			_idc_control->addPoint(_goal_disp, 2);
			_ioc_control->addPoint(_goal_rot, 2);
		}
		break;

	case SET_HT_TARGET_PRE_DEF_01:
		{
			_goal_disp.all(3);
			_goal_rot.ZRotate(90 * DEGREE);

			_idc_control->addPoint(_goal_disp, 2);
			_ioc_control->addPoint(_goal_rot, 2);
		}
		break;

	case SET_HT_TARGET_PRE_DEF_02:
		{
			_goal_disp.all(6);
			_goal_rot.ZRotate(-90 * DEGREE);

			_idc_control->addPoint(_goal_disp, 2);
			_ioc_control->addPoint(_goal_rot, 2);
		}
		break;

	case SET_HT_TARGET_INC_X:
		{
			_goal_disp[0] += 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_HT_TARGET_INC_Y:
		{
			_goal_disp[1] += 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_HT_TARGET_INC_Z:
		{
			_goal_disp[2] += 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_HT_TARGET_DEC_X:
		{
			_goal_disp[0] -= 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_HT_TARGET_DEC_Y:
		{
			_goal_disp[1] -= 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_HT_TARGET_DEC_Z:
		{
			_goal_disp[2] -= 0.1;
			_idc_control->addPoint(_goal_disp, 0.1);
		}
		break;

	case SET_WRENCH:
		_bApplyWrench = !_bApplyWrench;
		break;

	default:
		break;
	}

	return 0;
}

void control_SimpleFloatingBox_wrench::datanames(vector<string_type>& names, int channel)
{
	if(channel == 1)
	{
		names.push_back(_T("tx"));
		names.push_back(_T("ty"));
		names.push_back(_T("tz"));

		names.push_back(_T("lvx"));
		names.push_back(_T("lvy"));
		names.push_back(_T("lvz"));
		names.push_back(_T("avx"));
		names.push_back(_T("avy"));
		names.push_back(_T("avz"));
	}
	else if(channel == 2)
	{
		names.push_back(_T("torque1(tx)"));
		names.push_back(_T("torque2(ty)"));
		names.push_back(_T("torque3(tz)"));
		names.push_back(_T("torque4(mx)"));
		names.push_back(_T("torque5(my)"));
		names.push_back(_T("torque6(mz)"));
	}
}

void control_SimpleFloatingBox_wrench::collect(vector<double>& data, int channel)
{
	if(channel == 1)
	{
		for(int i = 9; i < 12; i++)
			data.push_back(_buf_htransform[i]);

		for(int i = 0; i < 6; i++)
			data.push_back(_buf_twist[i]);
	}
	else if(channel == 2)
	{
		for(int i = 0; i < 6; i++)
			data.push_back(_torque[i]);
	}
}

void control_SimpleFloatingBox_wrench::onSetInterestFrame(const TCHAR* name, const HTransform& T)
{
}

rControlAlgorithm* CreateControlAlgorithm(rDC& rdc)
{
	return new control_SimpleFloatingBox_wrench(rdc);
}
